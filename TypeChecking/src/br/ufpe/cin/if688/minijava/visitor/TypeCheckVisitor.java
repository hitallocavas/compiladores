package br.ufpe.cin.if688.minijava.visitor;

import br.ufpe.cin.if688.minijava.ast.And;
import br.ufpe.cin.if688.minijava.ast.ArrayAssign;
import br.ufpe.cin.if688.minijava.ast.ArrayLength;
import br.ufpe.cin.if688.minijava.ast.ArrayLookup;
import br.ufpe.cin.if688.minijava.ast.Assign;
import br.ufpe.cin.if688.minijava.ast.Block;
import br.ufpe.cin.if688.minijava.ast.BooleanType;
import br.ufpe.cin.if688.minijava.ast.Call;
import br.ufpe.cin.if688.minijava.ast.ClassDeclExtends;
import br.ufpe.cin.if688.minijava.ast.ClassDeclSimple;
import br.ufpe.cin.if688.minijava.ast.False;
import br.ufpe.cin.if688.minijava.ast.Formal;
import br.ufpe.cin.if688.minijava.ast.Identifier;
import br.ufpe.cin.if688.minijava.ast.IdentifierExp;
import br.ufpe.cin.if688.minijava.ast.IdentifierType;
import br.ufpe.cin.if688.minijava.ast.If;
import br.ufpe.cin.if688.minijava.ast.IntArrayType;
import br.ufpe.cin.if688.minijava.ast.IntegerLiteral;
import br.ufpe.cin.if688.minijava.ast.IntegerType;
import br.ufpe.cin.if688.minijava.ast.LessThan;
import br.ufpe.cin.if688.minijava.ast.MainClass;
import br.ufpe.cin.if688.minijava.ast.MethodDecl;
import br.ufpe.cin.if688.minijava.ast.Minus;
import br.ufpe.cin.if688.minijava.ast.NewArray;
import br.ufpe.cin.if688.minijava.ast.NewObject;
import br.ufpe.cin.if688.minijava.ast.Not;
import br.ufpe.cin.if688.minijava.ast.Plus;
import br.ufpe.cin.if688.minijava.ast.Print;
import br.ufpe.cin.if688.minijava.ast.Program;
import br.ufpe.cin.if688.minijava.ast.Statement;
import br.ufpe.cin.if688.minijava.ast.This;
import br.ufpe.cin.if688.minijava.ast.Times;
import br.ufpe.cin.if688.minijava.ast.True;
import br.ufpe.cin.if688.minijava.ast.Type;
import br.ufpe.cin.if688.minijava.ast.VarDecl;
import br.ufpe.cin.if688.minijava.ast.While;
import br.ufpe.cin.if688.minijava.symboltable.Class;
import br.ufpe.cin.if688.minijava.symboltable.Method;
import br.ufpe.cin.if688.minijava.symboltable.SymbolTable;

public class TypeCheckVisitor implements IVisitor<Type> {

	private SymbolTable symbolTable;
	private Class currClass;
	private Method currMethod;

	private boolean visita = false;

	public TypeCheckVisitor(SymbolTable st) {
		symbolTable = st;

	}

	// MainClass m;
	// ClassDeclList cl;
	public Type visit(Program n) {

		n.m.accept(this);

		for (int i = 0; i < n.cl.size(); i++) {
			n.cl.elementAt(i).accept(this);
		}

		return null;
	}

	// Identifier i1,i2;
	// Statement s;
	public Type visit(MainClass n) {
		currClass = symbolTable.getClass(n.i1.s);
		n.i1.accept(this);

		this.currMethod = symbolTable.getMethod("main", this.currClass.getId());
		this.visita = true;
		n.i2.accept(this);
		this.visita = false;
		currMethod = null;

		n.s.accept(this);

		currClass = null;
		return null;
	}

	// Identifier i;
	// VarDeclList vl;
	// MethodDeclList ml;
	public Type visit(ClassDeclSimple n) {
		currClass = symbolTable.getClass(n.i.toString());

		n.i.accept(this);

		for (int i = 0; i < n.vl.size(); i++) {

			n.vl.elementAt(i).accept(this);

		}

		for (int i = 0; i < n.ml.size(); i++) {

			n.ml.elementAt(i).accept(this);

		}

		currClass = null;

		return null;
	}

	// Identifier i;
	// Identifier j;
	// VarDeclList vl;
	// MethodDeclList ml;
	public Type visit(ClassDeclExtends n) {
		currClass = symbolTable.getClass(n.i.toString());

		n.i.accept(this);
		n.j.accept(this);

		for (int i = 0; i < n.vl.size(); i++) {

			n.vl.elementAt(i).accept(this);

		}

		for (int i = 0; i < n.ml.size(); i++) {

			n.ml.elementAt(i).accept(this);

		}

		currClass = null;

		return null;
	}

	// Type t;
	// Identifier i;
	public Type visit(VarDecl n) {
		n.t.accept(this);

		this.visita = true;

		n.i.accept(this);

		this.visita = false;

		return n.t;
	}

	// Type t;
	// Identifier i;
	// FormalList fl;
	// VarDeclList vl;
	// StatementList sl;
	// Exp e;
	public Type visit(MethodDecl n) {
		currMethod = symbolTable.getMethod(n.i.s, currClass.getId());
		Type t1 = n.t.accept(this);
		n.i.accept(this);

		Type t = symbolTable.getMethodType(n.i.s, currClass.getId());

		for (int i = 0; i < n.fl.size(); i++) {

			n.fl.elementAt(i).accept(this);

		}

		for (int i = 0; i < n.vl.size(); i++) {

			n.vl.elementAt(i).accept(this);

		}
		for (int i = 0; i < n.sl.size(); i++) {

			Statement s = n.sl.elementAt(i);
			s.accept(this);

		}

		if (n.e instanceof IdentifierExp) {

			this.visita = true;

		}

		Type expType = n.e.accept(this);

		this.visita = false;

		if (!symbolTable.compareTypes(t1, expType) && expType != null && t1 != null) {
			expType.accept(new PrettyPrintVisitor());
			t1.accept(new PrettyPrintVisitor());
			System.out.println();
		}

		currMethod = null;
		return t;

	}

	// Type t;
	// Identifier i;
	public Type visit(Formal n) {
		n.t.accept(this);
		this.visita = true;
		n.i.accept(this);
		this.visita = false;
		return null;
	}

	public Type visit(IntArrayType n) {
		return new IntArrayType();
	}

	public Type visit(BooleanType n) {
		return new BooleanType();
	}

	public Type visit(IntegerType n) {
		return new IntegerType();
	}

	// String s;
	public Type visit(IdentifierType n) {

		if (symbolTable.containsClass(n.s)) {

			return new IdentifierType(n.s);

		}

		return null;
	}

	// StatementList sl;
	public Type visit(Block n) {
		for (int i = 0; i < n.sl.size(); i++) {

			n.sl.elementAt(i).accept(this);

		}

		return null;
	}

	// Exp e;
	// Statement s1,s2;
	public Type visit(If n) {
		if (n.e instanceof IdentifierExp) {

			this.visita = true;

		}

		Type expType = n.e.accept(this);

		if (expType != null) {
			n.s1.accept(this);
			n.s2.accept(this);
		}

		this.visita = false;

		return null;
	}

	// Exp e;
	// Statement s;
	public Type visit(While n) {
		Type expType = n.e.accept(this);

		if (n.e instanceof IdentifierExp) {
			this.visita = true;
		}

		if (!(expType instanceof BooleanType) && expType != null) {

			System.out.println("n�o eh boolean");

		}

		n.s.accept(this);

		return null;

	}

	// Exp e;
	public Type visit(Print n) {
		if (n.e instanceof IdentifierExp) {
			this.visita = true;
		}
		Type expType = n.e.accept(this);
		this.visita = false;

		if (expType == null)
			return null;
		if (!(expType instanceof Type)) {
			n.e.accept(new PrettyPrintVisitor());

		}
		return null;
	}

	// Identifier i;
	// Exp e;
	public Type visit(Assign n) {
		this.visita = true;
		Type idType = n.i.accept(this);
		this.visita = false;
		if (n.e instanceof IdentifierExp) {
			this.visita = true;
		}
		Type expType = n.e.accept(this);
		this.visita = false;
		if (idType == null) {
			return null;
		}
		if (expType == null) {
			return null;
		}
		if (!symbolTable.compareTypes(idType, expType)) {
			System.out.println("Erro de tipos");
		}

		return null;
	}

	// Identifier i;
	// Exp e1,e2;
	public Type visit(ArrayAssign n) {
		this.visita = true;
		Type idType = n.i.accept(this);
		this.visita = false;
		if (n.e1 instanceof IdentifierExp) {
			this.visita = true;
		}
		Type expType1 = n.e1.accept(this);
		this.visita = false;
		if (n.e2 instanceof IdentifierExp) {
			this.visita = true;
		}
		Type expType2 = n.e2.accept(this);
		this.visita = false;
		if (idType == null | expType1 == null | expType2 == null) {
			return null;
		}
		if (!(idType instanceof IntArrayType)) {
			System.out.println("N�o � array");
		}
		if (!(expType1 instanceof IntegerType)) {
			n.e1.accept(new PrettyPrintVisitor());
			System.out.println(" n�o eh inteiro");
		}
		if (!(expType2 instanceof IntegerType)) {
			n.e2.accept(new PrettyPrintVisitor());
			System.out.println(" n�o eh inteiro");
		}
		return null;
	}

	// Exp e1,e2;
	public Type visit(And n) {
		if (n.e1 instanceof IdentifierExp) {
			this.visita = true;
		}
		Type expType1 = n.e1.accept(this);
		this.visita = false;
		if (n.e2 instanceof IdentifierExp) {
			this.visita = true;
		}
		Type expType2 = n.e2.accept(this);
		this.visita = false;
		if (expType1 == null | expType2 == null)
			return null;
		if (!(expType1 instanceof BooleanType)) {
			n.e1.accept(new PrettyPrintVisitor());
			System.out.println(" n�o eh boolean");
		}
		if (!(expType2 instanceof BooleanType)) {

			n.e2.accept(new PrettyPrintVisitor());
			System.out.println(" n�o eh boolean");
		}
		return new BooleanType();
	}

	// Exp e1,e2;
	public Type visit(LessThan n) {
		if (n.e1 instanceof IdentifierExp) {
			this.visita = true;
		}
		Type expType1 = n.e1.accept(this);
		this.visita = false;
		if (n.e2 instanceof IdentifierExp) {
			this.visita = true;
		}
		Type expType2 = n.e2.accept(this);
		this.visita = false;
		if (expType1 == null | expType2 == null)
			return null;
		if (!(expType1 instanceof IntegerType)) {
			n.e1.accept(new PrettyPrintVisitor());
			System.out.println(" n�o eh inteiro");
		}
		if (!(expType2 instanceof IntegerType)) {
			n.e2.accept(new PrettyPrintVisitor());
			System.out.println(" n�o eh intiero");
		}
		return new BooleanType();
	}

	// Exp e1,e2;
	public Type visit(Plus n) {
		if (n.e1 instanceof IdentifierExp) {
			this.visita = true;
		}
		Type expType1 = n.e1.accept(this);
		this.visita = false;
		if (n.e2 instanceof IdentifierExp) {
			this.visita = true;
		}
		Type expType2 = n.e2.accept(this);
		this.visita = false;
		if (expType1 == null | expType2 == null)
			return null;
		if (!(expType1 instanceof IntegerType)) {

			n.e1.accept(new PrettyPrintVisitor());

			System.out.println(" n�o eh intiero");
		}

		if (!(expType2 instanceof IntegerType)) {

			n.e2.accept(new PrettyPrintVisitor());
			System.out.println(" nao eh inteiro");
		}
		return new IntegerType();
	}

	// Exp e1,e2;
	public Type visit(Minus n) {
		if (n.e1 instanceof IdentifierExp) {
			this.visita = true;
		}
		Type expType1 = n.e1.accept(this);
		this.visita = false;

		if (n.e2 instanceof IdentifierExp) {
			this.visita = true;
		}
		Type expType2 = n.e2.accept(this);
		this.visita = false;

		if (expType1 == null | expType2 == null)
			return null;

		if (!(expType1 instanceof IntegerType)) {

			n.e1.accept(new PrettyPrintVisitor());
			System.out.println(" nao eh inteiro");
		}

		if (!(expType2 instanceof IntegerType)) {
			n.e2.accept(new PrettyPrintVisitor());
			System.out.println(" nao eh inteiro");
		}
		return new IntegerType();
	}

	// Exp e1,e2;
	public Type visit(Times n) {
		if (n.e1 instanceof IdentifierExp) {
			this.visita = true;
		}

		Type expType1 = n.e1.accept(this);

		this.visita = false;

		if (n.e2 instanceof IdentifierExp) {
			this.visita = true;
		}

		Type expType2 = n.e2.accept(this);

		this.visita = false;

		if (expType1 == null | expType2 == null)
			return null;

		if (!(expType1 instanceof IntegerType)) {

			n.e1.accept(new PrettyPrintVisitor());
			System.out.println(" nao eh inteiro");

		}
		if (!(expType2 instanceof IntegerType)) {

			n.e2.accept(new PrettyPrintVisitor());
			System.out.println(" nao eh inteiro");

		}
		return new IntegerType();
	}

	// Exp e1,e2;
	public Type visit(ArrayLookup n) {

		if (n.e1 instanceof IdentifierExp) {

			this.visita = true;

		}
		Type exp1Type = n.e1.accept(this);

		this.visita = false;
		if (n.e2 instanceof IdentifierExp) {
			this.visita = true;
		}
		Type exp2Type = n.e2.accept(this);
		this.visita = false;

		if (exp1Type != null && exp2Type != null) {
			if (!(exp1Type instanceof IntArrayType)) {

				n.e1.accept(new PrettyPrintVisitor());
				System.out.println(" nao eh array de inteiro");
			}

			if (!(exp2Type instanceof IntegerType)) {

				n.e2.accept(new PrettyPrintVisitor());
				System.out.println(" nao eh array de inteiro");
			}

			return new IntegerType();
		}
		return null;
	}

	// Exp e;
	public Type visit(ArrayLength n) {
		if (n.e instanceof IdentifierExp) {
			this.visita = true;
		}
		Type expType = n.e.accept(this);
		this.visita = false;

		if (expType != null) {
			if (!(expType instanceof IntArrayType)) {

				n.e.accept(new PrettyPrintVisitor());
				System.out.println(" nao eh array de inteiro");

			} else
				return new IntegerType();
		}
		return null;
	}

	// this.m().g()
	// Exp e;
	// Identifier i;
	// ExpList el;
	public Type visit(Call n) {
		if (n.e instanceof IdentifierExp) {

			this.visita = true;
		}

		Type expType = n.e.accept(this);

		this.visita = false;

		String className = "";

		if (expType instanceof IdentifierType) {

			className = ((IdentifierType) expType).s;

		} else {

			System.out.println("");

		}

		if (symbolTable.getMethod(n.i.toString(), className) == null) {
			System.out.print("Deu merda");
		} else {
			Type methodType = symbolTable.getMethod(n.i.toString(), className).type();
			Method method = symbolTable.getMethod(n.i.toString(), className);

			int sizeMethodParams = 0;

			while (true) {
				if (method.getParamAt(sizeMethodParams) != null)
					sizeMethodParams++;
				else
					break;
			}

			if (n.el.size() == sizeMethodParams) {

				for (int i = 0; i < n.el.size(); i++) {

					visita = true;
					Type paramType = n.el.elementAt(i).accept(this);
					visita = false;
					Type methodParamType = method.getParamAt(i).type();

					if (!(symbolTable.compareTypes(paramType, methodParamType))) {

						System.out.println("Erro de Tipo ");

					}
				}
			}
			return methodType;
		}

		return null;
	}

	// int i;
	public Type visit(IntegerLiteral n) {
		return new IntegerType();
	}

	public Type visit(True n) {
		return new BooleanType();
	}

	public Type visit(False n) {
		return new BooleanType();
	}

	// String s;
	public Type visit(IdentifierExp n) {
		Type t = symbolTable.getVarType(currMethod, currClass, n.s);
		if (t == null) {
			System.out.println("Erro de express�o");
		}
		return t;
	}

	public Type visit(This n) {
		return currClass.type();
	}

	// Exp e;
	public Type visit(NewArray n) {
		if (n.e instanceof IdentifierExp) {
			this.visita = true;
		}
		Type expType = n.e.accept(this);
		this.visita = false;

		if (expType != null) {
			if (!(expType instanceof IntegerType)) {
				n.e.accept(new PrettyPrintVisitor());
				System.out.println(" nao eh inteiro");
			} else
				return new IntArrayType();
		}
		return null;
	}

	// Identifier i;
	public Type visit(NewObject n) {
		Method aux = currMethod;
		currMethod = null;
		Type idType = n.i.accept(this);
		currMethod = aux;

		return idType;
	}

	// Exp e;
	public Type visit(Not n) {
		if (n.e instanceof IdentifierExp) {
			this.visita = true;
		}
		Type expType = n.e.accept(this);
		this.visita = false;

		if (expType != null) {
			if (!(expType instanceof BooleanType)) {
				n.e.accept(new PrettyPrintVisitor());
				System.out.println(" n�o eh  boolean");
			} else
				return new BooleanType();
		}

		return null;
	}

	// String s;
	public Type visit(Identifier n) {
		if (this.visita) {

			return symbolTable.getVarType(currMethod, currClass, n.toString());

		} else {

			if (currClass != null && currMethod == null && !symbolTable.containsClass(n.toString())) {

				System.out.println("Not Found");
				return null;

			}

			if (currClass == null && symbolTable.getClass(currClass.getId()).parent() != null) {

				if (!symbolTable.getClass(symbolTable.getClass(currClass.getId()).parent()).containsMethod(n.toString())
						&& !symbolTable.getClass(currClass.getId()).containsMethod(n.toString())) {
					System.out.println("N�o foi encontrado nem simbolo nem casses");
					return null;
				}
			} else if (!symbolTable.getClass(currClass.getId()).containsMethod(n.toString())) {
				System.out.println("simbolo nao encontrado");
				return null;
			}
		}

		return new IdentifierType(n.toString());
	}

	private String getTypeName(Type t) {
		if (t != null) {
			if (t instanceof BooleanType)
				return "Boolean";
			else if (t instanceof IdentifierType)
				return ((IdentifierType) t).toString();
			else if (t instanceof IntArrayType)
				return "int []";
			else
				return "int";
		}
		return "null";
	}
}
