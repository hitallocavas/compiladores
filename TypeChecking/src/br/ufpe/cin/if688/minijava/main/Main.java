package br.ufpe.cin.if688.minijava.main;

import br.ufpe.cin.if688.minijava.ast.BooleanType;
import br.ufpe.cin.if688.minijava.ast.ClassDeclExtends;
import br.ufpe.cin.if688.minijava.ast.ClassDeclList;
import br.ufpe.cin.if688.minijava.ast.ClassDeclSimple;
import br.ufpe.cin.if688.minijava.ast.Identifier;
import br.ufpe.cin.if688.minijava.ast.IdentifierType;
import br.ufpe.cin.if688.minijava.ast.IntegerLiteral;
import br.ufpe.cin.if688.minijava.ast.IntegerType;
import br.ufpe.cin.if688.minijava.ast.MainClass;
import br.ufpe.cin.if688.minijava.ast.MethodDeclList;
import br.ufpe.cin.if688.minijava.ast.Print;
import br.ufpe.cin.if688.minijava.ast.Program;
import br.ufpe.cin.if688.minijava.ast.VarDecl;
import br.ufpe.cin.if688.minijava.ast.VarDeclList;
import br.ufpe.cin.if688.minijava.visitor.BuildSymbolTableVisitor;
import br.ufpe.cin.if688.minijava.visitor.PrettyPrintVisitor;
import br.ufpe.cin.if688.minijava.visitor.TypeCheckVisitor;

public class Main {

	public static void main(String[] args) {
		MainClass main = new MainClass(
				new Identifier("Compiladores"), 
				new Identifier("Leopoldo"), 
				new Print(new IntegerLiteral(5))
		);
		
		VarDeclList n0 = new VarDeclList();
		n0.addElement(new VarDecl(
			new BooleanType(),
			new Identifier("adiciona")
		));
		
		VarDeclList n1 = new VarDeclList();
		n1.addElement(new VarDecl(
			new BooleanType(),
			new Identifier("adiciona")
		));
		
		VarDeclList n2 = new VarDeclList();
		n2.addElement(new VarDecl(
			new BooleanType(),
			new Identifier("adiciona")
		));
		
		System.out.println("PS. ------------> Deveria ter dado duas merdas acima");
		
		VarDeclList deviaTerDadoMerda = new VarDeclList();
		n2.addElement(new VarDecl(
			new BooleanType(),
			new Identifier("adiciona1")
		));
		
		System.out.println("PS. ------------> N�o pode dar merda");
		
		
		
		
		MethodDeclList mdl = new MethodDeclList();
		
		ClassDeclSimple A = new ClassDeclSimple(
					new Identifier("sopradarmerda"), n0, mdl
		);
		
		ClassDeclSimple B = new ClassDeclSimple(
				new Identifier("sopradarmerda"), n0, mdl
		);
	
		
		
		ClassDeclList cdl = new ClassDeclList();
		cdl.addElement(A);
		cdl.addElement(B);

		Program p = new Program(main, cdl);
		
		PrettyPrintVisitor ppv = new PrettyPrintVisitor();
		BuildSymbolTableVisitor bs = new BuildSymbolTableVisitor();
		bs.visit(p);
		TypeCheckVisitor tc = new TypeCheckVisitor(bs.getSymbolTable());
		tc.visit(p);
		ppv.visit(p);
		
	}
}
